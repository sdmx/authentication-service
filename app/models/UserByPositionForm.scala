package models

object UserByPositionForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(
    orgId: Long,
    posId: List[Long],
  )

  val form = Form(mapping(
    "org_id" -> longNumber,
    "pos_id" -> list(longNumber),
  )(Data.apply)(Data.unapply))
}
