package models

import com.github.t3hnar.bcrypt._

object UserDbForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(
    name: String,
    email: String,
    password: String,
    roles: Option[List[String]] = None,
  ) {
    def asModel = UserDbForm.toModel(this)
    def merge(data: UserDb) = data.copy(
      name = name,
      email = email,
      password = Some(password.bcrypt(generateSalt)),
      roles = roles,
    )
  }

  val form = Form(mapping(
    "name" -> nonEmptyText,
    "email" -> nonEmptyText,
    "password" -> nonEmptyText,
    "roles" -> optional(list(text)),
  )(Data.apply)(Data.unapply))

  def toModel(data: Data) = new UserDb(
    name = data.name,
    email = data.email,
    password = Some(data.password.bcrypt(generateSalt)),
    roles = data.roles,
  )
}
