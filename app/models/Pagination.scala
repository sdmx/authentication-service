package models

import play.api.libs.json._

class Pagination[A](
  var data: Traversable[A] = List(),
  var totalCount: Long = 0,
  var limit: Option[Long] = None,
  var offset: Option[Long] = None
) {
  def asJson(implicit writes: Writes[Traversable[A]]): JsObject = {
    val limitVal = if (limit.isDefined) JsNumber(limit.get) else JsNull
    val offsetVal = if (offset.isDefined) JsNumber(offset.get) else JsNull

    JsObject(Seq(
      "limit" -> limitVal,
      "offset" -> offsetVal,
      "totalCount" -> JsNumber(totalCount),
      "data" -> Json.toJson(data)
    ))
  }
}