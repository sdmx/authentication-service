package models

import play.api.libs.json._

case class Role(code: String, name: String)

object Role {
  implicit val format: OFormat[Role] = Json.format[Role]

  val compiler = Role("compiler", "Compiler")
  val admin = Role("admin", "Administrator")
  val executive = Role("executive", "Executive")
  val guest = Role("guest", "Guest")

  val all = Seq(compiler, admin, guest, executive)

  def exists(code: String): Boolean = all.exists(_.code == code)
}
