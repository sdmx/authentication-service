package models

import play.api.data.validation._

object PasswordResetForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(password: String, passwordConfirm: String)

  val passwordConstraint: Constraint[String] = Constraint("constraints.password")({ value =>
    val errors =
      if ("\\W".r.findFirstIn(value).isEmpty) {
        Seq(ValidationError("Password must contain at least one special character"))
      }
      else if ("[A-Z]".r.findFirstIn(value).isEmpty) {
        Seq(ValidationError("Password must contain at least one capital letter"))
      }
      else if ("\\d".r.findFirstIn(value).isEmpty) {
        Seq(ValidationError("Password must contain number"))
      }
      else Nil

    if (errors.isEmpty) Valid else Invalid(errors)
  })

  val form = Form(
    mapping(
      "password" -> nonEmptyText(minLength = 6).verifying(passwordConstraint),
      "password_confirm" -> nonEmptyText
    )(Data.apply)(Data.unapply)
      verifying("Password Confirmation Failed", data => data.password == data.passwordConfirm)
  )
}
