package models

object LoginForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(username: String, password: String)

  val form = Form(mapping(
    "username" -> nonEmptyText,
    "password" -> nonEmptyText
  )(Data.apply)(Data.unapply))
}
