package models

object AuthenticateForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(clientId: String, redirectUrl: String)

  val form = Form(mapping(
    "client_id" -> nonEmptyText,
    "redirect_url" -> nonEmptyText
  )(Data.apply)(Data.unapply))
}
