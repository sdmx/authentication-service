package models

import java.sql.Timestamp

import components.LdapConnection
import javax.inject._
import org.joda.time.DateTime
import org.ldaptive._
import org.ldaptive.auth._
import org.ldaptive.extended._
import play.api._
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.collection.JavaConverters._
import scala.concurrent._

@Singleton
class UserLdapRepository @Inject()(
  dbConfigProvider: DatabaseConfigProvider,
  config: Configuration,
  ldapConn: LdapConnection,
)(implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  implicit def dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  implicit def seqMapper =
    MappedColumnType.base[Seq[String], String](
      list => list.mkString(","),
      links => links.split("\\,").toSeq
    )

  private class UserTable(tag: Tag) extends Table[UserLdap](tag, "users_ldap") {
    def uid = column[String]("uid", O.PrimaryKey)
    def name = column[String]("name")
    def email = column[String]("email")
    def roles = column[Option[Seq[String]]]("roles")
    def orgId = column[Option[Long]]("org_id")
    def posId = column[Option[Long]]("pos_id")
    def lastLogin = column[Option[DateTime]]("last_login")
    def created = column[DateTime]("created")
    def deleted = column[Option[DateTime]]("deleted")

    def * = (
      uid, name, email, orgId, posId, roles, lastLogin, created, deleted
    ) <> ((UserLdap.apply _).tupled, UserLdap.unapply)
  }

  private val table = TableQuery[UserTable]

  val idField = "uid"

  def list(
    q: Option[String] = None,
    limit: Option[Long] = Some(100),
    offset: Option[Long] = Some(0),
    sort: Option[Seq[(String, String)]] = None
  ): Pagination[UserLdap] = {
    val conn = ldapConn.pool.getConnection

    try {
      conn.open()

      val baseDn = config.get[String]("ldap.default.baseDn")
      val query = "(objectclass=inetOrgPerson)"
      val result = new SearchOperation(conn).execute(new SearchRequest(baseDn, query)).getResult
      val entries = result.getEntries.asScala

      // todo: merge with db
      new Pagination[UserLdap](toModel(entries), entries.size, limit, offset)
    }
    catch {
      case e: Exception =>
        Logger.error(e.getMessage)
        e.printStackTrace()
        new Pagination
    }
    finally {
      conn.close()
    }
  }

  def create(data: UserLdapForm.Data): Future[Option[UserLdap]] = {
    val conn = ldapConn.pool.getConnection

    try {
      conn.open()

      // create ldap entry
      val user = data.asModel
      val entry = toLdapEntry(user)
      new AddOperation(conn).execute(new AddRequest(entry.getDn, entry.getAttributes))

      // set password
      data.password match {
        case Some(password) =>
          val req = new PasswordModifyRequest(entry.getDn)
          req.setNewPassword(new Credential(password))

          new PasswordModifyOperation(conn).execute(req)

        case _ =>
      }

      // make association on db
      // db.run(table returning table into ((item, _) => Some(item)) += user)
      db.run(table += user).map(_ => Some(user))
    }
    catch {
      case e: Exception =>
        Logger.error(e.getMessage)
        Future(None)
    }
    finally {
      conn.close()
    }
  }

  def find(uid: String): Future[Option[UserLdap]] = {
    val conn = ldapConn.pool.getConnection

    try {
      val baseDn = config.get[String]("ldap.default.baseDn")
      val result = new SearchOperation(conn).execute(new SearchRequest(baseDn, s"(uid=$uid)")).getResult

      associate(result.getEntry)
    }
    catch {
      case e: Exception =>
        Logger.error(e.getMessage)
        e.printStackTrace()
        Future(None)
    }
    finally conn.close()
  }

  def findByPosition(orgId: Long, posId: Long): Future[Seq[UserLdap]] = {
    db.run(table.filter(row => row.orgId === orgId && row.posId === posId).result)
  }

  def findByPosition(orgId: Long, posId: Seq[Long]): Future[Seq[UserLdap]] = {
    db.run(table.filter(row => row.orgId === orgId && row.posId.inSet(posId)).result)
  }

  def update(uid: String, user: UserLdap): Future[Int] = {
    val conn = ldapConn.pool.getConnection
    val oldDn = toLdapEntry(user.copy(uid = uid)).getDn
    val entry = toLdapEntry(user)

    try {
      conn.open

      // Update Attributes
      new ModifyOperation(conn).execute(
        new ModifyRequest(
          oldDn,
          new AttributeModification(AttributeModificationType.REPLACE, entry.getAttribute("cn")),
          new AttributeModification(AttributeModificationType.REPLACE, entry.getAttribute("sn")),
          new AttributeModification(AttributeModificationType.REPLACE, entry.getAttribute("mail"))
        )
      )

      // Update UID
      if (user.uid != uid) {
        new ModifyDnOperation(conn).execute(new ModifyDnRequest(oldDn, entry.getDn))
      }

      // Update user mapping
      db.run(table.filter(_.uid === uid).update(user)).map(status => {
        Logger.debug(s"User Updated $user")
        status
      })
    }
    catch {
      case e: Exception =>
        Logger.error(e.getMessage)
        e.printStackTrace()
        Future(0)
    }
    finally conn.close()
  }

  def changePassword(uid: String, oldPassword: String, newPassword: String): Boolean = {
    val conn = ldapConn.pool.getConnection

    try {
      conn.open
      new PasswordModifyOperation(conn).execute(
        new PasswordModifyRequest(getDn(uid), new Credential(oldPassword), new Credential(newPassword))
      )
      true
    }
    catch {
      case e: Exception =>
        Logger.error("Failed to change password on LDAP")
        Logger.error(e.getMessage)
        e.printStackTrace()
        false
    }
    finally conn.close()
  }

  def setPassword(uid: String, password: String): Boolean = {
    val conn = ldapConn.pool.getConnection

    try {
      conn.open

      // set password
      val req = new PasswordModifyRequest(getDn(uid))
      req.setNewPassword(new Credential(password))

      new PasswordModifyOperation(conn).execute(req)

      true
    }
    catch {
      case e: Exception =>
        Logger.error("Failed to set password on LDAP")
        Logger.error(e.getMessage)
        e.printStackTrace()
        false
    }
    finally conn.close()
  }

  def delete(uid: String): Future[Int] = {
    find(uid).flatMap {
      case Some(user) =>
        db.run(table.filter(_.uid === uid)
          .update(user.copy(deleted = Some(DateTime.now()))))

      case _ => Future(0)
    }
  }

  def forceDelete(uid: String): Future[Int] = {
    val conn = ldapConn.pool.getConnection

    try {
      conn.open

      find(uid).flatMap {
        case Some(user) =>
          new DeleteOperation(conn).execute(new DeleteRequest(toLdapEntry(user).getDn))
          db.run(table.filter(_.uid === uid).delete)

        case _ => Future(0)
      }
    }
    catch {
      case e: Exception =>
        Logger.error(e.getMessage)
        e.printStackTrace()
        Future(0)
    }
    finally conn.close()
  }

  /**
    * Create or Find User LDAP association on database
    * @param entry LdapEntry
    * @return
    */
  def associate(entry: LdapEntry): Future[Option[UserLdap]] = {
    val userTmp = toModel(entry)

    db.run(table.filter(_.uid === userTmp.uid).result.headOption).flatMap {
      case Some(data) => Future(Some(data))
      case None => 
        // db.run(table returning table into ((item, _) => Some(item)) += userTmp)
        db.run(table += userTmp).map(_ => Some(userTmp))
    }
  }

  val authenticator: Authenticator = {
    val filter = config.get[String]("ldap.default.userFilter")
    val baseDn = config.get[String]("ldap.default.baseDn")

    val resolver = new SearchDnResolver(ldapConn.getConnectionFactory)
    resolver.setBaseDn(baseDn)
    resolver.setUserFilter(filter)

    val handler = new BindAuthenticationHandler(ldapConn.getConnectionFactory)

    new Authenticator(resolver, handler)
  }

  def authenticate(identity: LoginForm.Data): Future[Option[UserLdap]] = {
    try {
      val response = authenticator.authenticate(
        new AuthenticationRequest(
          identity.username,
          new Credential(identity.password),
          "mail", "sn", "uid", "cn"
        )
      )

      if (response.getResult) {
        associate(response.getLdapEntry).map {
          case None => None
          case Some(user) =>
            db.run(table.filter(_.uid === user.uid).update(user.copy(lastLogin = Some(DateTime.now()))))
            Some(user)
        }
      }
      else
        Future(None)
    }
    catch {
      case e: Exception =>
        Logger.error(e.getMessage)
        e.printStackTrace()
        Future(None)
    }
  }

  def toModel(entry: LdapEntry): UserLdap = {
    UserLdap(
      uid = Option(entry.getAttribute(idField)).map(_.getStringValue).getOrElse(""),
      email = Option(entry.getAttribute("mail")).map(_.getStringValue).getOrElse(""),
      name = Option(entry.getAttribute("sn")).map(_.getStringValue).getOrElse("")
    )
  }

  def toModel(entries: Iterable[LdapEntry]): Iterable[UserLdap] = {
    for (entry <- entries) yield toModel(entry)
  }

  def toLdapEntry(user: UserLdap): LdapEntry = {
    new LdapEntry(getDn(user.uid),
      new LdapAttribute(idField, user.uid),
      new LdapAttribute("objectClass", "inetOrgPerson"),
      new LdapAttribute("mail", user.email),
      new LdapAttribute("sn", user.name),
      new LdapAttribute("cn", user.uid)
    )
  }

  def getDn(uid: String): String = {
    val baseDn = config.get[String]("ldap.default.baseDn")
    s"$idField=$uid,$baseDn"
  }
}
