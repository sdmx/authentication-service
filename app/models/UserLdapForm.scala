package models

object UserLdapForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(
    uid: String,
    name: String,
    email: String,
    roles: Option[List[String]] = None,
    password: Option[String] = None,
    orgId: Option[Long] = None,
    posId: Option[Long] = None,
  ) {
    def asModel = UserLdapForm.toModel(this)
    def merge(data: UserLdap) = data.copy(
      uid = uid,
      name = name,
      email = email,
      roles = roles,
      orgId = orgId,
      posId = posId,
    )
  }

  val form = Form(mapping(
    "uid" -> nonEmptyText,
    "name" -> nonEmptyText,
    "email" -> nonEmptyText,
    "roles" -> optional(list(text)),
    "password" -> optional(text),
    "orgId" -> optional(longNumber),
    "posId" -> optional(longNumber),
  )(Data.apply)(Data.unapply))

  def toModel(data: Data) = new UserLdap(
    uid = data.uid,
    name = data.name,
    email = data.email,
    roles = data.roles,
    orgId = data.orgId,
    posId = data.posId,
  )
}
