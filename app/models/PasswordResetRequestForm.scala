package models

object PasswordResetRequestForm {
  import play.api.data.Form
  import play.api.data.Forms._

  case class Data(email: String)

  val form = Form(mapping("email" -> email)(Data.apply)(Data.unapply))
}
