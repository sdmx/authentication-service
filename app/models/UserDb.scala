package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json.{Json, OFormat}

case class UserDb(
  id: Option[Long] = None,
  name: String,
  email: String,
  password: Option[String] = None,
  roles: Option[Seq[String]] = None,
  status: Char = UserDb.Status.unverified,
  lastLogin: Option[DateTime] = None,
  created: DateTime = DateTime.now(),
  deleted: Option[DateTime] = None,
) {
  def asUser = UserDb.toUser(this)
}

object UserDb extends OptionJsonFormat with CharJsonFormat with DateTimeJsonFormat {
  implicit val format: OFormat[UserDb] = Json.format[UserDb]

  object Status {
    val unverified = '0'
    val active = '1'
    val blocked = '2'
  }

  def toUser(userDb: UserDb): User = User(
    uid = userDb.id.getOrElse(0).toString,
    userType = User.UserType.db,
    name = userDb.name,
    email = userDb.email,
    roles = userDb.roles,
    lastLogin = userDb.lastLogin,
    created = userDb.created,
    deleted = userDb.deleted,
  )
}

