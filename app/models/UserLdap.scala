package models

import models.traits._
import org.joda.time.DateTime
import play.api.libs.json._

case class UserLdap(
  uid: String,
  name: String,
  email: String,
  orgId: Option[Long] = None,
  posId: Option[Long] = None,
  roles: Option[Seq[String]] = None,
  lastLogin: Option[DateTime] = None,
  created: DateTime = DateTime.now(),
  deleted: Option[DateTime] = None,
) {
  def asUser = UserLdap.toUser(this)
}

object UserLdap extends OptionJsonFormat with DateTimeJsonFormat {
  implicit val format: OFormat[UserLdap] = Json.format[UserLdap]

  def toUser(userLdap: UserLdap): User = User(
    uid = userLdap.uid,
    userType = User.UserType.ldap,
    name = userLdap.name,
    email = userLdap.email,
    orgId = userLdap.orgId,
    posId = userLdap.posId,
    roles = userLdap.roles,
    lastLogin = userLdap.lastLogin,
    created = userLdap.created,
    deleted = userLdap.deleted,
  )
}
