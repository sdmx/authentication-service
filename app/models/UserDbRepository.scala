package models

import java.sql.Timestamp

import javax.inject._
import org.joda.time.DateTime
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent._
import scala.concurrent.duration.Duration

@Singleton
class UserDbRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  implicit def dateTimeMapper =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  implicit def seqMapper =
    MappedColumnType.base[Seq[String], String](
      list => list.mkString(","),
      links => links.split("\\,").toSeq
    )

  private class UserTable(tag: Tag) extends Table[UserDb](tag, "users") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def email = column[String]("email")
    def password = column[Option[String]]("password")
    def roles = column[Option[Seq[String]]]("roles")
    def status = column[Char]("status")
    def lastLogin = column[Option[DateTime]]("last_login")
    def created = column[DateTime]("created")
    def deleted = column[Option[DateTime]]("deleted")

    def * = (
      id, name, email, password, roles, status, lastLogin, created, deleted
    ) <> ((UserDb.apply _).tupled, UserDb.unapply)
  }

  private val table = TableQuery[UserTable]

  def create(o: UserDb): Future[UserDb] = db.run(
    table returning table.map(_.id) into ((item, id) => item.copy(id = id)) += o
  )

  def find(id: Long): Future[Option[UserDb]] = db.run(table.filter(_.id === id).result.headOption)

  def findByEmail(email: String): Future[Option[UserDb]] = db.run(table.filter(_.email === email).result.headOption)

  def update(id: Long, o: UserDb): Future[UserDb] = db.run(
    table.filter(_.id === id).update(o)
  ).map(_ => o)

  def delete(id: Long): Future[Int] = db.run(table.filter(_.id === id).delete)

  def list(
    q: Option[String] = None,
    limit: Option[Long] = Some(100),
    offset: Option[Long] = Some(0),
    sort: Option[Seq[(String, String)]] = None
  ): Future[Pagination[UserDb]] = db.run {
    var query = table.sortBy(_.id.desc)

    // filter by key
    q.foreach(value => query = table.filter(_.name.like(s"%$value%")))

    // total results
    val totalCount = Await.result(db.run(query.length.result), Duration.Inf)

    // pagination
    offset.foreach(value => query = query.drop(value))
    limit.foreach(value => query = query.take(value))

    query.result.map(data => new Pagination(data, totalCount, limit, offset))
  }
}
