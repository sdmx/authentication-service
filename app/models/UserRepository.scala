package models

import javax.inject._
import scala.concurrent._

@Singleton
class UserRepository @Inject()(
  userLdapRepository: UserLdapRepository,
  userDbRepository: UserDbRepository,
)(implicit ec: ExecutionContext) {

  def find(uid: String, userType: String): Future[Option[User]] = {
    userType match {
      case User.UserType.ldap => userLdapRepository.find(uid).map(_.map(_.asUser))
      case User.UserType.db => userDbRepository.find(uid.toLong).map(_.map(_.asUser))
      case _ => Future(None)
    }
  }
}
