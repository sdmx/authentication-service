package models

import models.traits.{DateTimeJsonFormat, OptionJsonFormat}
import org.joda.time.DateTime
import play.api.libs.json._

case class User (
  uid: String,
  userType: String,
  name: String,
  email: String,
  orgId: Option[Long] = None,
  posId: Option[Long] = None,
  roles: Option[Seq[String]] = None,
  lastLogin: Option[DateTime] = None,
  created: DateTime = DateTime.now(),
  deleted: Option[DateTime] = None,
)

object User extends OptionJsonFormat with DateTimeJsonFormat {
  implicit val format: OFormat[User] = Json.format[User]

  object UserType {
    val ldap = "ldap"
    val db = "db"
  }
}
