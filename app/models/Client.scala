package models

import play.api.libs.json._

case class Client(
  code: String,
  name: String,
  roles: Option[Seq[String]] = None,
)

object Client extends traits.OptionJsonFormat {
  implicit val format: OFormat[Client] = Json.format[Client]

  val auth = Client("aut", "Authentication Server")
  val rstudio = Client("rst", "RStudio")
  val admin = Client("adm", "Administrator", Some(Seq(Role.admin.code)))
  val externalPortal = Client("exp", "Public Portal")
  val internalPortal = Client("inp", "Internal Portal")
  val soenda = Client("snd", "Data Analytic Information System", Some(Seq(Role.compiler.code)))
  val geospatialAnalytic = Client("gsa", "Geospatial Analytic")
  val dashboardExecutive = Client("dbe", "Dashboard Executive", Some(Seq(Role.executive.code)))

  val all = List(
    auth,
    rstudio,
    admin,
    externalPortal,
    internalPortal,
    soenda,
    geospatialAnalytic,
    dashboardExecutive,
  )

  def check(code: String): Boolean = all.exists(_.code == code)
}
