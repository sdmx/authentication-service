package models.traits

import org.joda.time.DateTime
import play.api.libs.json.{Reads, Writes}

trait DateTimeJsonFormat {
  implicit val jodaReads: Reads[DateTime] = Reads.jodaDateReads("yyyy-MM-dd HH:mm:ss")
  implicit val jodaWrites: Writes[DateTime] = Writes.jodaDateWrites("yyyy-MM-dd HH:mm:ss")
}
