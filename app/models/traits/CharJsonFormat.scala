package models.traits

import play.api.libs.json._

trait CharJsonFormat {
  implicit def charReads: Reads[Char] = {
    case JsString(value) => JsSuccess(value.charAt(0))
    case _ => JsError("error.expecting.stringChar")
  }
  implicit def charWrites: Writes[Char] = (value: Char) => JsString(value.toString)
}
