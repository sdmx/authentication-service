package components.provider

import models.{LoginForm, User}

import scala.concurrent.Future

trait GuardProvider {
  def login(identity: LoginForm.Data): Future[Option[User]]
}
