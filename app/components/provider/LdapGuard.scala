package components.provider

import models._
import play.api.{Configuration, Logger}

import scala.concurrent.{ExecutionContext, Future}

class LdapGuard(config: Configuration, repo: UserLdapRepository)(implicit ec: ExecutionContext) extends GuardProvider {

  def login(identity: LoginForm.Data): Future[Option[User]] = {
    Logger.debug(s"Login LDAP: $identity")

    repo.authenticate(identity).map {
      case Some(userLdap) => Some(userLdap.asUser)
      case _ => None
    }
  }
}
