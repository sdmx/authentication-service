package components.provider

import models._
import play.api.{Configuration, Logger}

import scala.concurrent.{ExecutionContext, Future}
import com.github.t3hnar.bcrypt._
import org.joda.time.DateTime

class DbGuard(config: Configuration, repo: UserDbRepository)(implicit ec: ExecutionContext) extends GuardProvider {

  def login(identity: LoginForm.Data): Future[Option[User]] = {
    Logger.debug(s"Login DB: $identity")

    repo.findByEmail(identity.username).map {
      case Some(userDb) =>
        val password = userDb.password.getOrElse("")

        if (identity.password.isBcrypted(password)) {
          userDb.id.map(id => repo.update(id, userDb.copy(lastLogin = Some(DateTime.now()))))
          Some(userDb.asUser)
        }
        else None // password doesn't match

      case _ => None
    }
  }
}
