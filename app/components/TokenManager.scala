package components

import java.util.Base64

import com.github.nscala_time.time.Imports.{DateTime, _}
import io.jsonwebtoken.{JwtException, Jwts, SignatureAlgorithm}
import javax.inject.{Inject, Singleton}
import models.User
import play.api.{Configuration, Logger}

import scala.collection.JavaConverters._

@Singleton
class TokenManager @Inject()(config: Configuration) {

  def encodedKey = Base64.getEncoder.encodeToString(config.get[String]("play.http.secret.key").getBytes)

  def generateToken(
    user: User,
    subject: String,
    issuer: Option[String] = None,
    expTime: Period = 30.day
  ): String = {
    val createdAt = DateTime.now()
    val expiredAt = DateTime.now() + expTime
    var claims = Map[String, AnyRef](
      "sub" -> subject,
      "iat" -> createdAt.toDate,
      "exp" -> expiredAt.toDate,
      "ust" -> user.userType,
      "uid" -> user.uid
    )

    user.roles.foreach(roles => claims = claims + ("rol" -> roles.mkString(",")))
    issuer.foreach(iss => claims = claims + ("iss" -> iss))

    val jwt = Jwts.builder().setClaims(claims.asJava)
      .signWith(SignatureAlgorithm.HS256, encodedKey)

    jwt.compact()
  }

  def verify(token: String) = {
    try {
      Some(Jwts.parser.setSigningKey(encodedKey).parseClaimsJws(token))
    }
    catch {
      case e: JwtException =>
        Logger.error(e.getMessage)
        None
    }
  }
}
