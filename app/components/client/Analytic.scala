package components.client

import com.softwaremill.sttp._
import javax.inject.Inject
import models.traits.{DateTimeJsonFormat, OptionJsonFormat}
import play.api.libs.json.{Json, OFormat}
import play.api.{Configuration, Logger}

object Analytic {

  implicit val backend = HttpURLConnectionBackend()

  class Client @Inject()(config: Configuration) {

    def login(username: Option[String] = None, password: Option[String] = None) = {
      sttp
        .post(url("login"))
        .body(Map(
          "login" -> username.getOrElse(config.get[String]("client.analytic.username")),
          "password" -> password.getOrElse(config.get[String]("client.analytic.password"))
        ))
        .send()
    }

    def createUser(user: User): Option[User]= {
      try {
        val res = sttp
          .cookies(login())
          .post(url("admin/users/create"))
          .body(Map("user" -> Json.toJson(user).toString()))
          .send()

        if (res.body.isLeft) None else Some(user)
      } catch {
       case e: Exception =>
        Logger.error("Failed to create user on Data Analytic")
        Logger.error(e.toString)
        None
      }
    }

    def updateUser(uid: String, user: User) = {
      try {
        val data = user.copy(login = uid)
        val res = sttp
          .cookies(login())
          .post(url("admin/users/edit"))
          .body(Map("user" -> Json.toJson(data).toString()))
          .send()

        if (res.body.isLeft) {
          Logger.error("Failed to edit user on Data Analytic")
          Logger.error(res.body.toString)
          None
        }
        else
          Some(data)
      } catch {
       case e: Exception =>
        Logger.error("Failed to edit user on Data Analytic")
        Logger.error(e.toString)
        None
      }
    }

    def deleteUser(uid: String) = {
      try {
        val res = sttp
          .cookies(login())
          .post(url("admin/users/delete"))
          .body(Map("login" -> uid))
          .send()

        res.body.isRight
      } catch {
       case e: Exception =>
        Logger.error(e.toString)
        false
      }
    }

    def getUser(uid: String) = {
      sttp
        .cookies(login())
        .get(url("admin/users/get", Map("login" -> uid)))
        .send()
    }

    def url(path: String = "", queries: Map[String, String] = Map()) = {
      var query = ""

      if (queries.nonEmpty) {
        query = "?" + queries.map(q => s"${q._1}=${q._2}").mkString("&")
      }

      uri"${config.get[String]("client.analytic.endpoint")}/$path$query"
    }
  }

  case class User (
    login: String,
    displayName: String,
    email: String,
    password: Option[String] = None,
    confirmPassword: Option[String] = None,
    userProfile: String = Profile.dataScientist,
    groups: Seq[String] = Seq(Group.dataTeam),
    sourceType: String = "LOCAL",
  )

  object User extends OptionJsonFormat with DateTimeJsonFormat {
    implicit val format: OFormat[User] = Json.format[User]
  }

  object Group {
    val administrator = "administrator"
    val dataTeam = "data_team"
    val readers = "readers"
    val all = Seq(administrator, dataTeam, readers)
  }

  object Profile {
    val dataScientist = "DATA_SCIENTIST"
    val dataAnalyst = "DATA_ANALYST"
    val reader = "READER"
    val all = Seq(dataScientist, dataAnalyst, reader)
  }
}
