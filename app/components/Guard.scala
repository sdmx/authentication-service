package components

import components.provider._
import javax.inject.{Inject, Singleton}
import models.{LoginForm, User, UserDbRepository, UserLdapRepository}
import play.api.Configuration

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

@Singleton
class Guard @Inject()(
  config: Configuration,
  userLdapRepo: UserLdapRepository,
  userDbRepo: UserDbRepository,
)(implicit ec: ExecutionContext) {

  def login(data: LoginForm.Data, provider: Option[GuardProvider] = None): Future[Option[User]] = {
    provider match {
      case None =>
        var authUser: Option[User] = None

        for (providerName <- Seq("ldap", "db")) {
          if (authUser.isEmpty) {
            val user = Await.result(getProvider(providerName).login(data), Duration.Inf)

            if (user.isDefined) {
              authUser = user
            }
          }
        }

        Future(authUser)

      case Some(authProvider) => authProvider.login(data)
    }
  }

  def getProvider(providerName: String = "db"): GuardProvider = {
    providerName match {
      case "ldap" => new LdapGuard(config, userLdapRepo)
      case _ => new DbGuard(config, userDbRepo)
    }
  }
}
