package components

import java.time.Duration

import javax.inject.{Inject, Singleton}
import org.ldaptive._
import org.ldaptive.pool._
import play.api.{Configuration, Logger}

@Singleton
class LdapConnection @Inject()(config: Configuration) {

  val pool = {
    val poolConfig = new PoolConfig
    poolConfig.setMinPoolSize(1)
    poolConfig.setMaxPoolSize(2)
    poolConfig.setValidateOnCheckOut(true)
    poolConfig.setValidateOnCheckIn(true)
    poolConfig.setValidatePeriodically(false)

    val pool = new BlockingConnectionPool
    pool.setPoolConfig(poolConfig)
    pool.setBlockWaitTime(Duration.ofSeconds(30))
    pool.setValidator(new SearchValidator)
    pool.setPruneStrategy(new IdlePruneStrategy)
    pool.setConnectionFactory(getConnectionFactory)
    pool.initialize()

    new PooledConnectionFactory(pool)
  }

  def getConnectionFactory: DefaultConnectionFactory = {
    val connConfig = new ConnectionConfig(config.get[String]("ldap.default.url"))
    val bindDn = config.get[String]("ldap.default.bindDn")
    val bindPassword = config.get[String]("ldap.default.bindPassword")
    connConfig.setConnectionInitializer(new BindConnectionInitializer(bindDn, new Credential(bindPassword)))
    connConfig.setUseStartTLS(false)
    connConfig.setConnectTimeout(Duration.ofSeconds(30))
    connConfig.setResponseTimeout(Duration.ofSeconds(30))

    new DefaultConnectionFactory(connConfig)
  }
}
