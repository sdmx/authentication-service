package controllers

import javax.inject._
import models._
import play.api.data.{Form, FormError}
import play.api.libs.json.{Json, Writes}
import play.api.mvc._

import scala.concurrent._

@Singleton
class UserDbController @Inject()(
  repo: UserDbRepository,
  cc: ControllerComponents,
)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def list = Action.async { implicit request =>
    val limit = Some(request.getQueryString("limit").fold(100L)(_.toLong))
    val offset = Some(request.getQueryString("offset").fold(0L)(_.toLong))

    repo
      .list(request.getQueryString("q"), limit, offset)
      .map { data => Ok(data.asJson) }
  }

  def retrieve(id: Long) = Action.async {
    repo.find(id).map {
      case Some(instance) => Ok(Json.toJson(instance))
      case _ => NotFound
    }
  }

  def create = Action.async { implicit request =>
    UserDbForm.form.bindFromRequest.fold(handleFormError, data => {
      repo.create(data.asModel.copy(status = UserDb.Status.active)).map(_ => Ok)
    })
  }

  def update(id: Long) = Action.async { implicit request =>
    UserDbForm.form.bindFromRequest.fold(handleFormError, data => {
      repo.find(id).map {
        case Some(instance) => repo.update(id, data.merge(instance)).synchronized(Ok)
        case _ => NotFound
      }
    })
  }

  def destroy(id: Long) = Action.async {
    repo.delete(id).map {
      case 1 => Ok
      case _ => NotFound
    }
  }

  private def handleFormError = (error: Form[UserDbForm.Data]) => {
    implicit val formErrorWrites: Writes[FormError] = (error: FormError) => {
      Json.obj("key" -> error.key, "message" -> error.messages)
    }

    Future.successful(Ok(Json.toJson(error.errors)))
  }
}
