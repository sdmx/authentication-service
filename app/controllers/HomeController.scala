package controllers

import components.TokenManager
import components.client.Analytic
import javax.inject._
import models.Client
import play.api._
import play.api.mvc._
import play.api.libs.json.Json

@Singleton
class HomeController @Inject()(
  cc: ControllerComponents,
  messagesAction: MessagesActionBuilder,
  tokenMgr: TokenManager,
  config: Configuration,
  analyticClient: Analytic.Client,
) extends AbstractController(cc) {

  def index() = Action { implicit request =>
    request.session.get("access_token") match {
      case Some(_) =>
        // Redirect to auth.defaultLoginRedirectUrl
        Redirect(config.get[String]("auth.defaultLoginRedirectUrl"))

      case _ => Redirect("/login")
    }
  }

  def client = Action { implicit request =>
    request.headers.get("Authorization") match {
      case Some(bearer) =>
        tokenMgr.verify(bearer.replaceAll("Bearer ", "")) match {
          case Some(_) => Ok(Json.toJson(Client.all))
          case _ => Unauthorized
        }

      case _ => Unauthorized
    }
  }

  def test = Action { implicit request =>
    val uid = "test1"
    val user = Analytic.User(
      login = uid,
      displayName = "test1 display namex",
      email = "test1@email.comx",
      password = Some("admin"),
      confirmPassword = Some("admin"),
      groups = Seq(Analytic.Group.dataTeam),
      userProfile = Analytic.Profile.dataScientist
    )

//    // create
//    analyticClient.createUser(user)

//    // update
//    analyticClient.updateUser(uid, user)

    // delete
    analyticClient.deleteUser(uid)

    Ok
  }
}
