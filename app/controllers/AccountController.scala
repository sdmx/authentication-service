package controllers

import components.TokenManager
import javax.inject._
import models._
import play.api._
import play.api.mvc._
import play.api.libs.mailer._
import com.github.nscala_time.time.Imports._
import com.github.t3hnar.bcrypt._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AccountController @Inject()(
  cc: ControllerComponents,
  tokenMgr: TokenManager,
  messagesAction: MessagesActionBuilder,
  accountRepo: UserDbRepository,
  mail: MailerClient,
  config: Configuration,
)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def resetPassword(token: String, clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    tokenMgr.verify(token) match {
      case Some(jws) =>
        val claims = jws.getBody
        val uid = claims.get("uid").toString

        if (uid.forall(Character.isDigit)) {
          val id = uid.toLong

          accountRepo.find(id).map {
            case Some(user) =>
              PasswordResetForm.form.bindFromRequest.fold(
                form => BadRequest(views.html.auth.passwordReset(form, token, Some(clientId), Some(redirectUrl))),
                data => {
                  accountRepo.update(id, user.copy(password = Some(data.password.bcrypt(generateSalt))))
                  Redirect(routes.AuthController.login(clientId, redirectUrl))
                    .flashing(("success", "Your password has been successfully changed."))
                }
              )

            case _ => BadRequest
          }
        }
        else Future(BadRequest)

      case _ => Future(BadRequest)
    }
  }

  def showPasswordResetForm(token: String, clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    tokenMgr.verify(token) match {
      case Some(jws) =>
        val claims = jws.getBody
        val uid = claims.get("uid").toString

        if (uid.forall(Character.isDigit)) {
          val id = uid.toLong

          accountRepo.find(id).map {
            case Some(_) =>
              Ok(views.html.auth.passwordReset(PasswordResetForm.form, token, Some(clientId), Some(redirectUrl)))

            case _ => BadRequest
          }
        }
        else Future(BadRequest)

      case _ => Future(BadRequest)
    }
  }

  def sendPasswordResetRequest(clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    PasswordResetRequestForm.form.bindFromRequest.fold(
      form => Future(BadRequest(views.html.auth.passwordResetRequest(form, Some(clientId), Some(redirectUrl)))),
      data => {
        accountRepo.findByEmail(data.email).map {
          case Some(user) =>
            val token = tokenMgr.generateToken(user.asUser, "verification", Some(clientId), 2.hours)

            mail.send(Email(
              s"${config.get[String]("app.name")} Password Reset",
              config.get[String]("play.mailer.from"),
              Seq(user.email),
              bodyHtml = Some(views.html.mail.passwordReset(token, Some(clientId), Some(redirectUrl)).toString)
            ))

            Redirect(routes.AccountController.showPasswordResetRequestForm(clientId, redirectUrl))
              .flashing(("success", "Reset password link has been sent to your email"))

          case _ =>
            Redirect(routes.AccountController.showPasswordResetRequestForm(clientId, redirectUrl))
              .flashing(("error", "Your email is not registered"))
        }
      }
    )
  }

  def showPasswordResetRequestForm(clientId: String, redirectUrl: String) = messagesAction { implicit request =>
    request.session.get("access_token") match {
      case Some(_) => Redirect("/")
      case _ => Ok(views.html.auth.passwordResetRequest(PasswordResetRequestForm.form, Some(clientId), Some(redirectUrl)))
    }
  }

  def verify(token: String, clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    tokenMgr.verify(token) match {
      case Some(jws) =>
        val claims = jws.getBody
        val uid = claims.get("uid").toString

        if (uid.forall(Character.isDigit)) {
          val id = uid.toLong

          accountRepo.find(id).map {
            case Some(user) =>
              if (user.status == UserDb.Status.unverified) {
                accountRepo.update(id, user.copy(status = UserDb.Status.active))
                Redirect(routes.AuthController.login(clientId, redirectUrl))
                  .flashing(
                    ("success", "Your email has been successfully verified."),
                    ("info", "Please login with your email and password to get started."),
                  )
              }
              else BadRequest

            case _ => BadRequest
          }
        }
        else Future(BadRequest)

      case _ => Future(BadRequest)
    }
  }

  def registerSuccess(userId: Long, clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    accountRepo.find(userId).map {
      case Some(user) =>
        if (user.status == UserDb.Status.unverified)
          Ok(views.html.auth.registerSuccess(user, clientId, redirectUrl))
        else
          BadRequest

      case _ => BadRequest
    }
  }

  def resendEmailVerification(userId: Long, clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    accountRepo.find(userId).map {
      case Some(user) =>
        if (user.status == UserDb.Status.unverified) {
          sendEmailVerification(user.asUser, Some(clientId), Some(redirectUrl))
          Redirect(routes.AccountController.registerSuccess(userId, clientId, redirectUrl))
        }
        else BadRequest

      case _ => BadRequest
    }
  }

  def register(clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    RegisterForm.form.bindFromRequest.fold(
      form => Future(BadRequest(views.html.auth.register(form, Some(clientId), Some(redirectUrl)))),
      data => {
        accountRepo.create(data.asModel).map(user => {
          sendEmailVerification(user.asUser, Some(clientId), Some(redirectUrl))
          Redirect(routes.AccountController.registerSuccess(user.id.getOrElse(0), clientId, redirectUrl))
        })
      }
    )
  }

  private def sendEmailVerification(user: User, clientId: Option[String], redirectUrl: Option[String])(implicit request: MessagesRequestHeader): Unit = {
    val token = tokenMgr.generateToken(user, "verification", clientId, 1.day)

    mail.send(Email(
      s"${config.get[String]("app.name")} Email Verification",
      config.get[String]("play.mailer.from"),
      Seq(user.email),
      bodyHtml = Some(views.html.mail.emailVerification(token, clientId, redirectUrl).toString)
    ))
  }

  def showRegisterForm(clientId: String, redirectUrl: String) = messagesAction { implicit request =>
    request.session.get("access_token") match {
      case Some(_) => Redirect("/")
      case _ => Ok(views.html.auth.register(RegisterForm.form, Some(clientId), Some(redirectUrl)))
    }
  }
}
