package controllers

import javax.inject._
import models._
import play.api.data.{Form, FormError}
import play.api.libs.json.{Json, Writes}
import play.api.mvc._
import play.api.{Logger}

import scala.concurrent._
import components.client.Analytic

@Singleton
class UserLdapController @Inject()(
  repo: UserLdapRepository,
  cc: ControllerComponents,
  analyticClient: Analytic.Client,
)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def list = Action { implicit request =>
    val limit = Some(request.getQueryString("limit").fold(100L)(_.toLong))
    val offset = Some(request.getQueryString("offset").fold(0L)(_.toLong))
    val data = repo.list(request.getQueryString("q"), limit, offset)

    Ok(data.asJson)
  }

  def create = Action.async { implicit request =>
    UserLdapForm.form.bindFromRequest.fold(handleFormError[UserLdapForm.Data], data => {
      analyticClient.createUser(analyticUser(data))

      repo.create(data).map {
        case Some(user) =>
          Ok(Json.toJson(user))
        case _ =>
          analyticClient.deleteUser(data.uid)
          Logger.error("Failed to create user on LDAP")
          InternalServerError
      }
    })
  }

  def update(uid: String) = Action.async { implicit request =>
    UserLdapForm.form.bindFromRequest.fold(handleFormError[UserLdapForm.Data], data => {
      repo.find(uid).map {
        case Some(user) =>
          analyticClient.updateUser(uid, analyticUser(data.copy(uid = uid)))
          repo.update(uid, data.asModel)

          // todo: handle inconsistent uid (client, ldap)
          // option: create a new one & delete the old one
          // risk: data loss

          // change password
          data.password match {
            case Some(password) => repo.setPassword(uid, password)
            case None =>
          }

          Ok
        case _ =>
          NotFound
      }
    })
  }

  def analyticUser(data: UserLdapForm.Data): Analytic.User = {
    val password = Some("admin")
//    val password = Some(data.uid.bcrypt(generateSalt))
    val roles = data.roles.getOrElse(Seq())
    val groups = if (roles.contains(Role.compiler.code))
      Seq(Analytic.Group.dataTeam, Analytic.Group.readers)
    else
      Seq(Analytic.Group.readers)

    Analytic.User(
      login = data.uid,
      displayName = data.name,
      email = data.email,
      password = password,
      confirmPassword = password,
      groups = groups
    )
  }

  def destroy(uid: String) = Action.async {
    analyticClient.deleteUser(uid)
    repo.forceDelete(uid).map {
      case 1 => Ok
      case _ => NotFound
    }
  }

  def retrieve(uid: String) = Action.async {
    repo.find(uid).map {
      case Some(instance) => Ok(Json.toJson(instance))
      case _ => NotFound
    }
  }

  def getUserByPosition = Action.async { implicit request =>
    UserByPositionForm.form.bindFromRequest.fold(handleFormError[UserByPositionForm.Data], data => {
      repo.findByPosition(data.orgId, data.posId).map { res =>
        Ok(Json.toJson(res.map(_.asUser)))
      }
    })
  }

  private def handleFormError[A] = (error: Form[A]) => {
    implicit val formErrorWrites: Writes[FormError] = (error: FormError) => {
      Json.obj("key" -> error.key, "message" -> error.messages)
    }

    Future.successful(Ok(Json.toJson(error.errors)))
  }
}
