package controllers

import components.{Guard, TokenManager}
import javax.inject._
import models._
import play.api.mvc._
import play.api.Logger
import play.api.libs.json.Json
import io.lemonlabs.uri._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthController @Inject()(
  messagesAction: MessagesActionBuilder,
  cc: ControllerComponents,
  guard: Guard,
  tokenMgr: TokenManager,
  userRepo: UserRepository,
)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  import models.LoginForm

  def authenticate = Action { implicit request =>
    AuthenticateForm.form.bindFromRequest.fold(
      _ => BadRequest,
      data => {
        if (Client.check(data.clientId)) {
          val url = request.session.get("access_token").fold(
            Url.parse("/login").addParams(
              "client_id" -> data.clientId,
              "redirect_url" -> data.redirectUrl
            )
          )(token => Url.parse(data.redirectUrl).addParams("token" -> token))

          Redirect(url.toString)
        }
        else Unauthorized // Unknown Client ID
      }
    )
  }

  def login(clientId: String, redirectUrl: String) = messagesAction.async { implicit request =>
    LoginForm.form.bindFromRequest.fold(
      form => Future(BadRequest(views.html.auth.login(form, Some(clientId), Some(redirectUrl)))),
      data => {
        guard.login(data).map {
          case Some(user) =>
            val clientId = request.queryString.get("client_id").flatMap(_.headOption)
            val accessToken = tokenMgr.generateToken(user, "login", clientId)

            val redirectUrl =
              clientId.fold[Option[String]](None)(value =>
                if (Client.check(value))
                  request.queryString.get("redirect_url").flatMap(_.headOption)
                else
                  None // Unknown Client ID
              ).fold("/")(url =>
                Url.parse(url).addParams("token" -> accessToken).toString
              )

            Redirect(redirectUrl).withSession("access_token" -> accessToken)

          case _ =>
            Redirect(routes.AuthController.showLoginForm(clientId, redirectUrl))
              .flashing("error" -> "Login Failed")
        }
      }
    )
  }

  def logout = Action { implicit request =>
    request.session.get("access_token") match {
      case Some(_) =>
        val clientId = request.queryString.get("client_id").flatMap(_.headOption)
        val redirectUrl = clientId.fold[Option[String]](None)(value =>
          if (Client.check(value))
            request.queryString.get("redirect_url").flatMap(_.headOption)
          else
            None // Unknown Client ID
        )

        Redirect(redirectUrl.getOrElse("/login")).withSession(request.session - "access_token")
      case _ => Redirect("/")
    }
  }

  def authInfo(token: String) = Action.async { implicit request =>
    tokenMgr.verify(token) match {
      case Some(jws) =>
        val claims = jws.getBody

        userRepo.find(claims.get("uid").toString, claims.get("ust").toString).map {
          case Some(user) => Ok(Json.toJson(user))
          case _ => NotFound
        }

      case _ => Future(Unauthorized)
    }
  }

  def showLoginForm(clientId: String, redirectUrl: String) = messagesAction { implicit request =>
    request.session.get("access_token") match {
      case Some(_) => Redirect("/")
      case _ => Ok(views.html.auth.login(LoginForm.form, Some(clientId), Some(redirectUrl)))
    }
  }

  def roles = Action { implicit request =>
    Ok(Json.toJson(Role.all))
  }
}
