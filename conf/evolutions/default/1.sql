# --- !Ups

create table `users_ldap` (
  `uid` varchar(100) primary key,
  `name` varchar(255) not null,
  `email` varchar(255) not null,
  `last_login` timestamp,
  `created` timestamp default now(),
  `deleted` timestamp,
  `roles` varchar(255),
  `org_id` int,
  `pos_id` int
);

create table `users` (
  `id` bigint primary key auto_increment,
  `name` varchar(255) not null,
  `email` varchar(255) not null,
  `password` varchar(255) not null,
  `last_login` timestamp,
  `status` char(1) not null default '0',
  `created` timestamp not null default now(),
  `deleted` timestamp,
  `roles` varchar(255)
);

create table `email_verification` (
  `user_id` bigint primary key references users(id) on update cascade on delete cascade,
  `key` varchar(100) not null,
  `created` timestamp not null default now()
);

create table `password_reset` (
  `user_id` bigint primary key references users(id) on update cascade on delete cascade,
  `key` varchar(100) not null,
  `created` timestamp not null default now()
);

create table `password_reset_ldap` (
  `uid` varchar(100) primary key references users_ldap(uid) on update cascade on delete cascade,
  `key` varchar(100) not null,
  `created` timestamp not null default now()
);

# --- !Downs

drop table if exists `email_verification`;
drop table if exists `password_reset`;
drop table if exists `users`;

drop table if exists `password_reset_ldap`;
drop table if exists `users_ldap`;
