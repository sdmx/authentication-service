FROM java:8-alpine

RUN apk update && apk add bash

COPY . /app/soenda/authentication-service
WORKDIR /app/soenda/authentication-service

COPY conf/application.docker.conf /app/soenda/authentication-service/conf/application.conf

# RUN sh -c "./sbt build"

EXPOSE 9000
ENTRYPOINT ["sh", "-c", "./sbt run 9000"]
